type = fancymenu_layout

layout-meta {
  identifier = connect_screen
  render_custom_elements_behind_vanilla = false
  last_edited_time = 1714385960955
  is_enabled = true
  randommode = false
  randomgroup = 1
  randomonlyfirsttime = false
  layout_index = 0
  [loading_requirement_container_meta:899e52c8-7a37-4687-add2-d76bbacc375c-1714385861604] = [groups:][instances:]
}

customization {
  action = backgroundoptions
  keepaspectratio = false
}

scroll_list_customization {
  preserve_scroll_list_header_footer_aspect_ratio = true
  render_scroll_list_header_shadow = true
  render_scroll_list_footer_shadow = true
  show_scroll_list_header_footer_preview_in_editor = false
  repeat_scroll_list_header_texture = false
  repeat_scroll_list_footer_texture = false
}

element {
  source = [source:local]/config/fancymenu/assets/praying-black.apng
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 6d9b9c3a-ffc3-4559-b786-e9d393d7adb8-1714385938862
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = mid-centered
  x = 244
  y = 118
  width = 133
  height = 100
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 13162dcf-233e-4836-b337-2c621920ab5b-1714385938862
  [loading_requirement_container_meta:13162dcf-233e-4836-b337-2c621920ab5b-1714385938862] = [groups:][instances:]
}

vanilla_button {
  button_element_executable_block_identifier = 08449857-c86f-440a-a19b-fd378595e623-1714385861606
  [executable_block:08449857-c86f-440a-a19b-fd378595e623-1714385861606][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = status
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 220
  y = 118
  width = 200
  height = 9
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 9f29ca99-166d-494c-8b2c-67871bf95307-1714385861606
  [loading_requirement_container_meta:9f29ca99-166d-494c-8b2c-67871bf95307-1714385861606] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 5b569839-008c-4794-87c9-6c57a56723b7-1714385861606
  [executable_block:5b569839-008c-4794-87c9-6c57a56723b7-1714385861606][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 400382
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 220
  y = 216
  width = 200
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = cff07156-8a18-43cd-b5fc-7b118fc44bd2-1714385861606
  [loading_requirement_container_meta:cff07156-8a18-43cd-b5fc-7b118fc44bd2-1714385861606] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

