// prority: 2

StartupEvents.registry(
    'item', e => {
        e.create('blue_ice_ingot')
        .unstackable()
        .glow(true)
        .rarity("epic")
        .tooltip("终极无敌蓝冰锭!可合成多个重要物品, 整合包最强材料!")
        .displayName("炫彩蓝冰锭")
    }
)