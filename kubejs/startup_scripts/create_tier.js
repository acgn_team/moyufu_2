// prority: 1

ItemEvents.toolTierRegistry(event => {
    event.add('blue_ice', tier => 
        {
            tier.uses = 12000
            tier.speed = 10
            tier.attackDamageBonus = 50
            tier.level = 5
            tier.enchantmentValue = 10
            tier.repairIngredient = 'kubejs:blue_ice_ingot'
        }
    );
})