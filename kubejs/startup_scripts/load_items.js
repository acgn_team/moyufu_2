// prority: 0

StartupEvents.registry('item', e => {

    // 蓝冰系列物品

    e.create('blue_ice_pickaxe', 'pickaxe')
        .tier('blue_ice')
        .glow(true)
        .rarity('epic')
        .displayName("蓝冰神镐")

    e.create('blue_ice_sword', 'sword')
        .tier('blue_ice')
        .attackDamageBaseline(100.0)
        .attackDamageBonus(25)
        .glow(true)
        .rarity('epic')
        .displayName("蓝冰大剑")

    e.create('blue_ice_axe', 'axe')
        .tier('blue_ice')
        .speed(200.0)
        .glow(true)
        .rarity('epic')
        .displayName("蓝冰巨斧")
    
    e.create('blue_ice_shovel', 'shovel')
        .tier('blue_ice')
        .speedBaseline(200.0)
        .speed(4)
        .glow(true)
        .rarity('epic')
        .displayName("蓝冰铲")
    
    e.create('blue_ice_hoe', 'hoe')
        .tier('blue_ice')
        .glow(true)
        .rarity('epic')
        .displayName("蓝冰锄")
        // .attackDamageBaseline(100)
        // .attackDamageBonus(20)
        
    //

    //

    //

    //

    //

    //

    //

    //

    //
})